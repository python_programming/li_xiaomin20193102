# 文件名 ： 格式化字符串实践
# 作  者 ： 20193102李潇敏
# 日  期 ： 2020.3.28

title = ["车次","出发站-到达站","出发时间","到达时间","历时"]
template = "{:<4} \t{:>}-{:<}\t\t{:>}\t\t{:^}\t\t{:^}"   #模板
#将更新的车票信息都放在列表中
ticket = ["","","","","",""]
none = True
while none:
    print("欢迎您更新车票信息！")
    print("请输入车次：")
    ticket[0] = input()
    print("请输入出发站：")
    ticket[1] = input()
    print("请输入到达站：")
    ticket[2] = input()
    print("请输入出发时间：")
    ticket[3] = input()
    print("请输入到达时间：")
    ticket[4] = input()
    print("请输入历时：")
    ticket[5] = input()

#将更新的信息按模板排列
    newticket = template.format(ticket[0],ticket[1],ticket[2],ticket[3],ticket[4],ticket[5])

    print("请查看更新后的信息！")
    print("车次  出发站-到达站\t   出发时间\t\t到达时间\t\t  历时")
    print("T40     长春-北京\t     00:12\t\t12:20\t\t12:08\nT298\t长春-北京\t\t00:06\t\t10:50\t\t10:44\nZ158\t长春-北京\t\t12:48\t\t21:06\t\t08:18")
    print(newticket,"\n")
    new = newticket

    print("还要更新么（要请输入1）")
    a = input()
    if a==1:
        none = False
    else:
        break


