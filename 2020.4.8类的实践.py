##文件名：类的对象实践
##时  间：2020.4.8
##姓  名：李潇敏

class Lesson:
    list_lesson = ["高数","英语","电路分析","大学物理"]

    def __init__(self,lesson):
        self.__lesson=lesson
    @property
    def lesson(self):
        return self.__lesson
    @lesson.setter
    def lesson(self,project):
        if project in Lesson.lest_lesson:
            self.__lesson="您选择的科目是"+project+"请稍后"
        else:
            self.__lesson="您选择的科目不存在"

learning=Lesson("高数")
print("请从",Lesson.list_lesson,"中挑选想查看的科目")
learning.show="高数"
print(learning.show)


class Gaoshu(Lesson):
    lesson="向量"
    def __init__(self):
        print("高数的知识点是",lesson,)
class English(Lesson):
    lesson="语法"
    def __init__(self):
        print("英语的知识点是",lesson)

gaoshu=Gaoshu()