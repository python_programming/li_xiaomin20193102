### 文件名：python数据库实验
### 作  者：李潇敏
### 日  期：2020.4.29

#导入pymysql模块
import pymysql
#调用commect（）函数产生connection连接对象
db = pymysql.connect(host = 'studypython',user = 'root',database = 'mrsoft',password = 'root',charset = "utf8")
#调用cursor（）方法，创建cursor对象
cursor = db.cursor()
data = ('lxm','高等数学','88','2')
#执行sql语句
sql = "insert into user(人员,课程,成绩,学分) values(%s,%s,%s)"

# 插入
data = [('191101', 'The Great Gatsby', '13.14', '1925-06-30'),

        ('191102', 'The Little Prince', '13.15', '1942-01-13')]

sql = 'insert into books (id,name,price,publish_time) value (%s,%s,%s,%s)'
cursor.executemany(sql, data)
# 查询
cursor.execute('select * from books')
# 修改
cursor.execute('update books set price = 13.14 where id = 191102')
# 删除
cursor.execute('delete from books where id = 191102')
cursor.execute('select * from books')
cursor.execute('select * from books')
cursor.execute(sql,data)

cursor.close()
db.close()


