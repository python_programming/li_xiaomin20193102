##文件名：计算器
##姓  名：李潇敏
##时  间：2020.4.21

print("====欢迎来到计算器====")
chioce2 = 1
while(chioce2 == 1):
    print("请选择功能：\n1.加法乘除计算\n2.求模运算\n3.进制转化\n4.阶乘\n5.累加")
    print("隐藏功能：\n6.九九乘法表\n请选择您要计算的类型的序号~")
    chioce1 = int(input())

##1.加法乘除计算
    none4 = True
    while(chioce1 == 1):
        while none4:
            if chioce1 == 1:
                list=[]
                print("请依次输入式子每一项（括号不能叠加，完成后输入over）")
                none2 = True
                i = 0
                while none2:
                    i = i+1
                    print("请输入算式第",i,"项：")
                    a = input()
                    if a == "over":
                        none2 = None
                    else:
                        list.append(a)
            print("\n请核查是否正确")
            for index,item in enumerate(list):
                print(item,end='')
            print("\n正确输入1，错误输入2")
            ans = int(input())
            if ans == 1:
                print("\n")
                break

        ##计算括号内的
        while("(" in list):
            kuohao1 = list.index("(")
            kuohao2 = list.index(")")

            kuohao_list = list[int(kuohao1)+1:int(kuohao2)]


            ##自定义计算函数
            def chengfa():
                if ('*' in kuohao_list):
                    cheng = [i for i, v in enumerate(kuohao_list) if v == '*']
                    cheng_list = kuohao_list[int(cheng[0]) - 1:int(cheng[0] + 2)]
                    result_cheng = str(int(cheng_list[0]) * int(cheng_list[2]))
                    kuohao_list.insert(int(cheng[0]) + 2, result_cheng)
                    del kuohao_list[int(cheng[0]) - 1:int(cheng[0] + 2)]
                return kuohao_list

            def chufa():
                if ('/' in kuohao_list):
                    cheng = [i for i, v in enumerate(kuohao_list) if v == '/']
                    cheng_list = kuohao_list[int(cheng[0]) - 1:int(cheng[0] + 2)]
                    result_cheng = str(int(cheng_list[0]) // int(cheng_list[2]))
                    kuohao_list.insert(int(cheng[0]) + 2, result_cheng)
                    del kuohao_list[int(cheng[0]) - 1:int(cheng[0] + 2)]
                return kuohao_list

            def jiafa():
                if ('+' in kuohao_list):
                    cheng = [i for i, v in enumerate(kuohao_list) if v == '+']
                    cheng_list = kuohao_list[int(cheng[0]) - 1:int(cheng[0] + 2)]
                    result_cheng = str(int(cheng_list[0]) + int(cheng_list[2]))
                    kuohao_list.insert(int(cheng[0]) + 2, result_cheng)
                    del kuohao_list[int(cheng[0]) - 1:int(cheng[0] + 2)]
                return kuohao_list

            def jianfa():
                if ('-' in kuohao_list):
                    cheng = [i for i, v in enumerate(kuohao_list) if v == '-']
                    cheng_list = kuohao_list[int(cheng[0]) - 1:int(cheng[0] + 2)]
                    result_cheng = str(int(cheng_list[0]) - int(cheng_list[2]))
                    kuohao_list.insert(int(cheng[0]) + 2, result_cheng)
                    del kuohao_list[int(cheng[0]) - 1:int(cheng[0] + 2)]
                return kuohao_list

            ##括号内计算
            while("*"in kuohao_list):
                kuohao_list = chengfa()
            while ("/" in kuohao_list):
                kuohao_list = chufa()
            while ("+" in kuohao_list):
                kuohao_list = jiafa()
            while ("-" in kuohao_list):
                kuohao_list = jianfa()

            list.insert(kuohao2 + 1, kuohao_list[0])
            del list[kuohao1:kuohao2+1]


        ##没有括号
        ##自定义计算没有括号函数
        def chengfa():
            if ('*' in list):
                cheng = [i for i, v in enumerate(list) if v == '*']
                cheng_list = list[int(cheng[0]) - 1:int(cheng[0] + 2)]
                result_cheng = str(int(cheng_list[0]) * int(cheng_list[2]))
                list.insert(int(cheng[0]) + 2, result_cheng)
                del list[int(cheng[0]) - 1:int(cheng[0] + 2)]
            return list

        def chufa():
            if ('/' in list):
                cheng = [i for i, v in enumerate(list) if v == '/']
                cheng_list = list[int(cheng[0]) - 1:int(cheng[0] + 2)]
                result_cheng = str(int(cheng_list[0]) / int(cheng_list[2]))
                list.insert(int(cheng[0]) + 2, result_cheng)
                del list[int(cheng[0]) - 1:int(cheng[0] + 2)]
            return list

        def jiafa():
            if ('+' in list):
                cheng = [i for i, v in enumerate(list) if v == '+']
                cheng_list = list[int(cheng[0]) - 1:int(cheng[0] + 2)]
                result_cheng = str(int(cheng_list[0]) + int(cheng_list[2]))
                list.insert(int(cheng[0]) + 2, result_cheng)
                del list[int(cheng[0]) - 1:int(cheng[0] + 2)]
            return list

        def jianfa():
            if ('-' in list):
                cheng = [i for i, v in enumerate(list) if v == '-']
                cheng_list = list[int(cheng[0]) - 1:int(cheng[0] + 2)]
                result_cheng = str(int(cheng_list[0]) - int(cheng_list[2]))
                list.insert(int(cheng[0]) + 2, result_cheng)
                del list[int(cheng[0]) - 1:int(cheng[0] + 2)]
            return list

        ##计算
        while("*"in list):
            list = chengfa()
        while ("/" in list):
            list = chufa()
        while ("+" in list):
            list = jiafa()
        while ("-" in list):
            list = jianfa()

        print("计算结果是：",int(list[0]))
        print("还要继续使用计算器么（继续输入1，退出输入2）")
        chioce2 = int(input())
        break

    while(chioce1 == 2):
        print("请分别输入参与运算的两个数")
        print("第一个数：")
        a = int(input())
        print("第二个数：")
        b = int(input())
        c = a//b
        d = a%b
        print(a,"除",b,"商",c,"余",d)
        print("还要继续使用计算器么（继续输入1，退出输入2）")
        chioce2 = int(input())
        break

    while(chioce1 == 3):
        jz = ['','','','']
        print("请选择进制：（2，8，10，16）")
        a = int(input())
        print("请输入数字：")
        b = str(input())
        if a == 2:
            jz10 = int(b,2)
        elif a == 8:
            jz10 = int(b,8)
        elif a == 10:
            jz10 = int(b)
        elif a == 16:
            jz10 = int(b, 16)
        jz[0] = bin(jz10)
        jz[1] = oct(jz10)
        jz.insert(2,jz10)
        jz[3] = hex(jz10)
        title = ["二进制", "八进制", "十进制", "十六进制"]
        template = "{:<6}  \t{:^6}   \t{:^6}   \t{:3}"
        new1 = template.format(title[0], title[1], title[2],title[3])
        new2 = template.format(jz[0], jz[1], jz[2],jz[3])
        print(new1)
        print(new2)
        print("还要继续使用计算器么（继续输入1，退出输入2）")
        chioce2 = int(input())
        break

    while(chioce1 == 4):
        print("请输入要阶乘的数：")
        a = int(input())
        b = a
        sum = 1
        while a != 0:
            sum = sum*a
            a = a - 1
        print(b,"的阶乘为",sum)
        print("还要继续使用计算器么（继续输入1，退出输入2）")
        chioce2 = int(input())
        break

    while(chioce1 == 5):
        print("请输入要累加的起始数(包括)：")
        a = int(input())
        print("请输入要累加的终止数（不包括）：")
        b = int(input())
        result = 0
        for i in range(a,b):
            result +=i
        print("结果为",result)
        print("还要继续使用计算器么（继续输入1，退出输入2）")
        chioce2 = int(input())
        break

    while(chioce1 == 6):
        print("恭喜触发隐藏功能！")
        for i in range(1,10):
            for j in range(1,i+1):
                print(str(j)+"x"+str(i)+"="+str(j*i)+"\t",end='')
            print(" ")
        print("还要继续使用计算器么（继续输入1，退出输入2）")
        chioce2 = int(input())
        break

    if chioce2 == 2:
        print("感谢您的使用！")
        break













