# 文件名 ： 成功决策树
# 作  者 ： 20193102李潇敏
# 日  期 ： 2020.3.11

none = True
while none:
    print("你想要成功么？（yes or no）")
    a1 = str(input("你的回答是："))
    if a1 == "yes":
        print("你会为了成功制定计划么？（yes or no）")
        a2 = str(input("你的回答是："))
        if a2 == "yes":
            print("你会为了成功付出行动么？（yes or no）")
            a3 = str(input("你的回答是："))
            if a3 == "yes":
                print("每天学习几个小时呢？")
                a4 = int(input("你的回答是："))
                if a4 > 6:
                    print("太棒啦！你一定会成功的！^-^")
                    none = False
                else:
                    print("离成功只有一步之遥，再努力一点吧~@_@~")
                    none = False
            else:
                print("仰望星空也要脚踏实地哦~@_@~")
                none = False
        else:
            print("良好的开端从小目标开始~@_@~")
            none = False
    else:
        print("加油！进步从改变思想开始~@_@~")
        none = False
