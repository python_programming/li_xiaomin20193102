# 文件名 ： 序列的应用实践
# 作  者 ： 20193102李潇敏
# 日  期 ： 2020.3.18

#第一道题
wj = [""]
print("请给您起一个震撼峡谷的称号！\n我是大名鼎鼎：")
a = input()
wj[0] = a                            #修改元素
print("欢迎 ",wj[0]," ，请您查看英雄！\n")
tk = ["程咬金","牛魔","项羽","白起","猪八戒","刘邦","廉颇","达摩","夏侯惇",""]
tk.append("钟无艳")                  #添加元素
zs = ["吕布","曹操","赵云","孙悟空","花木兰","宫本武藏","露娜",""]
del zs[1:2]                   #通过切片删除元素
ck = ["韩信","荆轲","兰陵王","李白","赵云",'橘右京','马超','百里玄策','花木兰',""]
fs = ["貂蝉"," 妲己","芈月"," 嬴政",'西施','安琪拉','小乔','干将莫邪','墨子','周瑜','妲己','扁鹊',""]
ss = ["狄仁杰","鲁班七号","孙尚香","后羿",'百里守约','成吉思汗','李元芳','马可波罗','鲁班七号','公孙离',""]
fz = ["孙膑","太乙真人","蔡文姬","庄周",'盾山','鬼谷子','大乔','张飞','姜子牙','瑶',""]
wzry = tk + zs + ck + fs + ss + fz  #通过序列相加添加元素

print(wzry)
print("\n\n")
print("王者荣耀中坦克英雄：")
for item in tk:
    print(item+"  ",end='')
print("\n王者荣耀中战士英雄：")
for item in zs:
    print(item+"  ",end='',)
print("\n王者荣耀中刺客英雄：")
for item in ck:
    print(item+"  ",end='')
print("\n王者荣耀中法师英雄：")
for item in fs:
    print(item+"  ",end='')
print("\n王者荣耀中射手英雄：")
for item in ss:
    print(item+"  ",end='')
print("\n王者荣耀中辅助英雄：")
for item in fz:
    print(item+"  ",end='')

print("\n\n欢迎来到王者荣耀！峡谷即将开放！\n")
print('您是否要对阵容进行修改？(yes or no)')
a = str(input("您的回答是："))
if a == "yes" or a == "y":
    print("您要修改的英雄类型是(坦克、战士、法师、刺客、辅助)：")
    b = str(input())
    if b == "坦克":
        print("您要添加新英雄还是删除原有英雄？（添加 or 删除）")
        c = str(input())
        if c == "添加":
            print("您要添加的英雄名是：")
            d = str(input())
            if d in tk:
                print("\n英雄已在阵容中!")

            else:
                tk.append(d)
                print("添加成功，英雄进入阵容！")

        else:
            print("您要删除的英雄名是：")
            d = str(input())
            if d in tk:
                tk.remove ("d")
                print("英雄已删除！")

    elif b =="战士":
        print("您要添加新英雄还是删除原有英雄？（添加 or 删除）")
        c = str(input())
        if c == "添加":
            print("您要添加的英雄名是：")
            d = str(input())
            if d in zs:
                print("英雄已在阵容中！")
            else:
                tk.append(d)
                print("添加成功，英雄进入阵容！")
        else:
            print("您要删除的英雄名是：")
            d = str(input())
            if d in zs:
                tk.remove ("d")
                print("英雄已删除！")
    elif b =="刺客":
        print("您要添加新英雄还是删除原有英雄？（添加 or 删除）")
        c = str(input())
        if c == "添加":
            print("您要添加的英雄名是：")
            d = str(input())
            if d in ck:
                print("英雄已在阵容中")
            else:
                ck.append("d")
                print("添加成功，英雄进入阵容！")
        else:
            print("您要删除的英雄名是：")
            d = str(input())
            if d in ck:
                ck.remove ("d")
                print("英雄已删除！")
    elif b =="射手":
        print("您要添加新英雄还是删除原有英雄？（添加 or 删除）")
        c = str(input())
        if c == "添加":
            print("您要添加的英雄名是：")
            d = str(input())
            if d in ss:
                print("英雄已在阵容中！")
            else:
                ss.append("d")
                print("添加成功，英雄进入阵容！")
        else:
            print("您要删除的英雄名是：")
            d = str(input())
            if d in ss:
                ss.remove ("d")
                print("英雄已删除！")
    elif b == "法师":
        print("您要添加新英雄还是删除原有英雄？（添加 or 删除）")
        c = str(input())
        if c == "添加":
            print("您要添加的英雄名是：")
            d = str(input())
            if d in fs:
                print("英雄已在阵容中")
            else:
                fs.append("d")
                print("添加成功，英雄进入阵容！")
        else:
            print("您要删除的英雄名是：")
            d = str(input())
            if d in fs:
                fs.remove("d")
                print("英雄已删除！")
    elif b == "辅助":
        print("您要添加新英雄还是删除原有英雄？（添加 or 删除）\n")
        c = str(input())
        if c == "添加":
            print("您要添加的英雄名是：")
            d = str(input())
            if d in fz:
                print("英雄已在阵容中")
            else:
                fz.append("d")
                print("添加成功，英雄进入阵容！")
        else:
            print("您要删除的英雄名是：")
            d = str(input())
            if d in fz:
                fz.remove("d")
                print("英雄已删除！")
    else:
        print("无法识别指令，请重新输入")
elif a == "no":
    wzry = tk + zs + ck + fs + ss + fz
    print(wzry)
    print("英雄就位！敌军还有五秒到达战场，请做好准备！")
else:
    print("无法识别指令，请重新输入")
print("\n\n")



print("\n\n\n"+"~"*20 + "\n\n\n")         #分割线

#第二道题

movie = [
    ('最好的我们',7.7),
    ('。。。。。。。',8.9),
    ('我和我的祖国',8.8),
    ('哪吒之魔童降世',9.0),
    ('千与千寻',8.7),
    ('少年的你',8.5),
    ('比悲伤更悲伤的故事',8.3),
    ('叶问4：完结篇',8.6),
    ('亲爱的新年好',8.2),
    ]
del movie[:1]          #用切片删除一个元素
movie.append( ('最好的我们',7.7))         #添加一个元素
movie[0] =  ('绿皮书',8.9)
print("\n2019年爱奇艺电影评分排名（部分）")
movie = sorted(movie,key = lambda movie:movie[1],reverse = True)
for idex,item in enumerate(movie):
    print(idex+1,"  电影名：",item[0],"\n    电影评分：",item[1])