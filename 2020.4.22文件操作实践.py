def mkdir(path):
    # 引入模块
    import os
    # 去除首位空格
    path = path.strip()
    # 去除尾部 \ 符号
    path = path.rstrip("\\")
    # 判断路径是否存在
    # 存在     True
    # 不存在   False
    isExists = os.path.exists(path)
    # 判断结果
    if not isExists:
        # 如果不存在则创建目录
        # 创建目录操作函数
        os.makedirs(path)
        print(path + ' 创建成功')
        return True
    else:
        # 如果目录存在则不创建，并提示目录已存在
        print(path + ' 目录已存在')
        return False


# 创建一个txt文件，文件名为mytxtfile,并向文件写入msg

def txt_create(name, msg):
    desktop_path = "D:\\python\\1931\\"  # 新创建的txt文件的存放路径

    full_path = desktop_path + name + '.txt'  # 也可以创建一个.doc的word文档

    file = open(full_path, 'a+')

    file.write(msg)

    # file.close()


# 任务一创建一个文件夹
# 定义要创建的目录
mkpath = "D:\\python\\1931\\"
# 调用函数
mkdir(mkpath)

# 任务二：创建一个文件并写入自己的信息
txt_create('20193102', '20193102李潇敏 python 61\n')

# 任务三：输入其他同学
txt_create('20193111', '20193111程心雨 python 59\n')

# 任务四：读取内容
filename = "D:\\python\\1931\\20193102.txt"
try:
    fp = open(filename, "r")
    print("%s 文件开始读取" % filename)
    content = fp.read()
    print(content)
    fp.close()
except IOError:
    print("文件打开失败，%s文件不存在"%filename)