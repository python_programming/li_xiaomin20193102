### 文件名：python数据库实验
### 作  者：李潇敏
### 日  期：2020.4.29

# noinspection PyUnresolvedReferences
import socket

s = socket.socket(socket.AF_INET,socket.SOCK_STREAM)
s.connect(('127.0.0.1',8005))
str = input("请输入要传输的内容")
s.sendall(str.encode())
data = s.recv(1024)
print(data.decode( ))
s.close()


