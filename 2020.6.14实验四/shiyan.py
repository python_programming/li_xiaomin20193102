
import os, sys, pygame

from pygame.locals import *

size = (700, 700)
white = (255, 255, 255)
block_group = []
isOver = False
color = (0,0,0)

ball = pygame.image.load('./ball1.png')
ballrect = ball.get_rect()
# print(ballrect.top,ballrect.bottom)
# exit()

speed = [0, 0]
clock = pygame.time.Clock()

background_group = [
    [0, 1, 1, 1, 0, 0, 0, 0, 0, 0],
    [0, 1, 1, 1, 0, 1, 1, 1, 1, 0],
    [0, 0, 0, 1, 0, 0, 0, 0, 1, 0],
    [0, 1, 1, 1, 0, 1, 1, 0, 1, 0],
    [0, 1, 0, 0, 0, 1, 1, 1, 1, 0],
    [0, 1, 0, 0, 1, 0, 0, 0, 0, 0],
    [0, 1, 0, 0, 1, 0, 1, 1, 1, 1],
    [0, 1, 0, 0, 1, 0, 0, 0, 0, 1],
    [0, 1, 0, 0, 1, 0, 1, 1, 1, 1],
    [0, 0, 0, 0, 1, 0, 0, 0, 0, 0]]


class Android:

    def __init__(self, img, rect, speed):


        self.ful_img = img
        self.imgs = [self.ful_img.subsurface(Rect((i * 64.5, 0), (70, 70)))
                     for i in range(11)]

        self.rect = rect
        self.speed = speed
        self.num = 0


    def update(self, screen, press_keys):
        global isOver

        self.num += 1
        if self.num % 11 == 0:
            self.num = 0


        if press_keys[K_ESCAPE]:
            exit()

        if press_keys[K_LEFT]:

            self.rect.left -= self.speed

            if self.rect.left <= 0:
                self.rect.left = 0

            for n in range(len(block_group)-1):
                if self.rect.top > block_group[n].rect.top - 70 and self.rect.bottom < block_group[n].rect.bottom + 70:
                    if self.rect.right > block_group[n].rect.right > self.rect.left:
                        self.rect.left += self.speed
                        break
            if self.num % 3 == 1 or self.num % 3 == 0:
                screen.blit(self.imgs[2], self.rect)
                return 0
            else:
                screen.blit(self.imgs[0], self.rect)
                return 0

        if press_keys[K_RIGHT]:

            self.rect.left += self.speed

            if self.rect.right >= 700:
                self.rect.right = 700

            for n in range(len(block_group)-1):
                if self.rect.top > block_group[n].rect.top - 70 and self.rect.bottom < block_group[n].rect.bottom + 70:
                    if self.rect.left < block_group[n].rect.left < self.rect.right:
                        self.rect.left -= self.speed
                        break


            if self.rect.top > block_group[len(block_group) - 1].rect.top - 70 and self.rect.bottom < block_group[len(block_group) - 1].rect.bottom + 70:
                if self.rect.left < block_group[len(block_group) - 1].rect.left < self.rect.right:
                    isOver = True

            if self.num % 3 == 1 or self.num % 3 == 0:
                screen.blit(self.imgs[1], self.rect)
                return 0
            else:
                screen.blit(self.imgs[0], self.rect)
                return 0

        if press_keys[K_UP]:

            self.rect.top -= self.speed

            if self.rect.top <= 0:
                self.rect.top = 0

            for n in range(len(block_group)-1):
                if self.rect.left > block_group[n].rect.left - 70 and self.rect.right < block_group[n].rect.right + 70:
                    if self.rect.bottom > block_group[n].rect.bottom > self.rect.top:
                        self.rect.top += self.speed
                        break


            if self.num % 3 == 1 or self.num % 3 == 0:
                screen.blit(self.imgs[9], self.rect)
                return 0
            else:
                screen.blit(self.imgs[0], self.rect)
                return 0

        if press_keys[K_DOWN]:

            self.rect.top += self.speed

            if self.rect.bottom >= 700:
                self.rect.bottom = 700

            for n in range(len(block_group)-1):
                if self.rect.left > block_group[n].rect.left - 70 and self.rect.right < block_group[n].rect.right + 70:
                    if self.rect.top < block_group[n].rect.top < self.rect.bottom:
                        self.rect.top -= self.speed
                        break

            if self.rect.left > block_group[len(block_group)-1].rect.left - 70 and self.rect.right < block_group[len(block_group)-1].rect.right + 70:
                if self.rect.top < block_group[len(block_group)-1].rect.top < self.rect.bottom:
                    isOver = True

            if self.num % 3 == 1 or self.num % 3 == 0:
                screen.blit(self.imgs[3], self.rect)
                return 0
            else:
                screen.blit(self.imgs[0], self.rect)
                return 0


        if 3 >= self.num >= 0 or self.num > 7:
            screen.blit(self.imgs[0], self.rect)
        elif self.num == 5 or self.num == 4:
            screen.blit(self.imgs[5], self.rect)
        elif self.num == 6 or self.num == 7:
            screen.blit(self.imgs[6], self.rect)

        return 0


class Block:
    def __init__(self, img, rect):
        self.img = img
        self.rect = rect


    def draw(self, screen):
        screen.blit(self.img, self.rect)


def drawBackground(screen):

    block_group.clear()


    block1_img = pygame.image.load('./block1.png').convert()

    for i in range(10):
        for j in range(10):
            if background_group[i][j] == 1:
                block = Block(block1_img, Rect(70*j, 70*i, 70, 70))
                block.draw(screen)
                block_group.append(block)


    block_final = Block(pygame.image.load('./block2.png').convert(), Rect(630, 630, 70, 70))
    block_final.draw(screen)
    block_group.append(block_final)


def game():

    os.environ['SDL_VIDEO_CENTERED'] = '1'
    speed_android = 5
    dwTime = 6
    r_android = Rect(0, 0, 70, 70)
    pygame.init()
    clock = pygame.time.Clock()
    screen = pygame.display.set_mode(size, 0, 32)
    android = pygame.image.load('./robot.png').convert_alpha()
    background1 = pygame.image.load('./background2.png').convert()
    Andr = Android(android, r_android, speed_android)
    pygame.display.set_caption("迷宫游戏")
    ball = pygame.image.load('./ball1.png')
    ballrect = ball.get_rect()
    # print(ballrect.top,ballrect.bottom)
    # exit()

    speed = [0, 0]
    clock = pygame.time.Clock()



    while True:
        if isOver:
            screen.blit(pygame.image.load('./over.jpg').convert(), (0, 0))
            pygame.display.update()
            for event in pygame.event.get():
                if event.type == QUIT:
                    pygame.quit()
                    sys.exit()
                if event.type == KEYDOWN:
                    exit()
            clock.tick(1000)
            continue
        screen.fill(white)
        screen.blit(background1, (0, 0))
        drawBackground(screen)
        for event in pygame.event.get():
            if event.type == QUIT:
                pygame.quit()
                sys.exit()

        ballrect = ballrect.move(speed)
        # 碰撞检测
        if ballrect.left < 0 or ballrect.right > 640:
            speed[0] = -speed[0]

        if ballrect.top < 0 or ballrect.bottom > 480:
            speed[1] = -speed[1]

        press_keys = pygame.key.get_pressed()
        Andr.update(screen, press_keys)
        pygame.display.update()
        clock.tick(dwTime)
    screen.fill(color)


if __name__ == "__main__":
    game()